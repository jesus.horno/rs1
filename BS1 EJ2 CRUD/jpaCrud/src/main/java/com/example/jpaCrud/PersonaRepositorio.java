package com.example.jpaCrud;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface PersonaRepositorio extends JpaRepository<Persona, Integer> {
    public ArrayList<Persona> findByUsuario(String nombre);
}
