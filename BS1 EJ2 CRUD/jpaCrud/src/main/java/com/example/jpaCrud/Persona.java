package com.example.jpaCrud;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import org.springframework.data.repository.CrudRepository;

@Entity
@Data
public class Persona {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id_persona;
    @Column(name="usu", nullable = false)
    private String usuario;
    @Column(name="pass", nullable = false)
    private String password;
    @Column(name="name", nullable = false)
    private String name;
    @Column(name="surname")
    private String surname;
    @Column(name="company_email", nullable = false)
    private String company_email;
    @Column(name="personal_email", nullable = false)
    private String personal_email;
    @Column(name="city", nullable = false)
    private String city;
    @Column(name="active", nullable = false)
    private boolean active;
    @Column(name="created_date", nullable = false)
    private Date created_date;
    @Column(name="imagen_url")
    private String imagen_url;
    @Column(name="termination_date")
    private Date termination_date;


}
