package com.example.jpaCrud.infraestructure.controller;

import com.example.jpaCrud.apllication.PersonaService;
import com.example.jpaCrud.domain.Persona;
import com.example.jpaCrud.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class ControladorBuscar {
    @Autowired
    PersonaService service;

    @GetMapping("/asignacion")
    public ArrayList<CreatePersonaOutputDTO> getPersona() throws Exception {
        return service.buscarALL();
    }

}
