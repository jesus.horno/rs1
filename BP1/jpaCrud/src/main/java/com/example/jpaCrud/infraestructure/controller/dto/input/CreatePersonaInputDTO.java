package com.example.jpaCrud.infraestructure.controller.dto.input;

import com.example.jpaCrud.domain.Persona;
import com.example.jpaCrud.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

@Data
public class CreatePersonaInputDTO implements Serializable {

    private int id_persona;
    private String usuario;
    private String password;
    private String name;
    private String surname;
    private String company_email;
    private String personal_email;
    private String city;
    private Boolean active;
    private Date created_date;
    private String imagen_url;
    private Date termination_date;

    public Persona personaDTO(CreatePersonaInputDTO p){
        Persona persona = new Persona();

//        if(p == null) return null;
//        p.setId_persona(this.getId_persona());
//
        persona.setUsuario(p.getUsuario());
        persona.setPassword(p.getPassword());
        persona.setName(p.getName());
        persona.setSurname(p.getSurname());
        persona.setCompany_email(p.getCompany_email());
        persona.setPersonal_email(p.getPersonal_email());
        persona.setCity(p.getCity());
        persona.setActive(p.getActive());
        persona.setCreated_date(p.getCreated_date());
        persona.setImagen_url(p.getImagen_url());
        persona.setTermination_date(p.getTermination_date());
        return persona;
    }
}
