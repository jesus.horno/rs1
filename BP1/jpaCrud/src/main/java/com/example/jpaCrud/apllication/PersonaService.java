package com.example.jpaCrud.apllication;

import com.example.jpaCrud.domain.Persona;
import com.example.jpaCrud.infraestructure.controller.dto.input.CreatePersonaInputDTO;
import com.example.jpaCrud.infraestructure.controller.dto.output.CreatePersonaOutputDTO;

import java.util.ArrayList;

public interface PersonaService {

    public Persona dtoEntrada(CreatePersonaInputDTO input);
    public CreatePersonaOutputDTO añadir(Persona p);
    public boolean comprobacionNulos(Persona perso);
    public CreatePersonaOutputDTO modificar(Persona p, int id) throws Exception;
    public void borrar(int id) throws Exception;
    public CreatePersonaOutputDTO buscarID(int id) throws Exception;
    public ArrayList<CreatePersonaOutputDTO> buscarALL();
    public ArrayList<CreatePersonaOutputDTO> buscarNombre(String nombre);

}
