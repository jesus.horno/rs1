package com.example.jpaCrud.Estudiante.infraestructure.controller;

import com.example.jpaCrud.Estudiante.aplication.EstudianteService;
import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input.AsignarAsignaturasInputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input.CreateEstudianteInputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.CreateEstudianteoOutputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.EstudianteAsignaturasAñadidasOutputDTO;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.input.EstudianteAsignaturaInputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import java.util.ArrayList;

@RestController
@RequestMapping("estudiante")
public class ControladorCRUD {
    @Autowired
    EstudianteService service;

    @PostMapping("/asignacion")
    public CreateEstudianteoOutputDTO añadirPersona(@RequestBody CreateEstudianteInputDTO createEstudianteInputDTO) {
        return service.añadirEstudiante(createEstudianteInputDTO);
    }

    @DeleteMapping("/borrado/{id}")
    public String DeletePersona(@PathVariable String id) throws Exception {
        return service.borrarEstudiante(id);
    }

    @GetMapping("/asignacion/{id}")
    public CreateEstudianteoOutputDTO getPersona(@PathVariable String id, @RequestParam(name="outputType") String output) throws Exception {
        return service.buscarID(id, output);
    }

    @GetMapping("/asignacion")
    public ArrayList<CreateEstudianteoOutputDTO> getPersona() throws Exception {
        return service.buscarALL();
    }

    @PutMapping("/asignacion/{id}")
    public CreateEstudianteoOutputDTO modificarPersona(@PathVariable String id, @RequestBody CreateEstudianteInputDTO input) throws Exception {
        return service.modificar(input,id);
    }

    @PutMapping("/añadir/{id}")
    public EstudianteAsignaturasAñadidasOutputDTO AñadirAsignaturaEstudiante(@PathVariable String id, @RequestBody AsignarAsignaturasInputDTO input) throws Exception {
        return service.AñadirAsignaturas(input,id);
    }

    @PutMapping("/eliminar/{id}")
    public EstudianteAsignaturasAñadidasOutputDTO EliminarAsignaturaEstudiante(@PathVariable String id, @RequestBody AsignarAsignaturasInputDTO input) throws Exception {
        return service.EliminarAsignaturas(input,id);
    }

}
