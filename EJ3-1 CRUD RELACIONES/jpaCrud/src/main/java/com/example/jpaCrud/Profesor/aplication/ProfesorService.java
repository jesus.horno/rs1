package com.example.jpaCrud.Profesor.aplication;

import com.example.jpaCrud.Profesor.domain.Profesor;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.input.ProfesorInputDTO;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.output.ProfesorOutputDTO;

import java.util.ArrayList;
import java.util.Optional;

public interface ProfesorService {

    public ProfesorOutputDTO añadirProfesor(ProfesorInputDTO input);
    public String borrarProfesor(String id);
    public ProfesorOutputDTO buscarID(String id);
    public ArrayList<ProfesorOutputDTO> buscarALL();
    public ProfesorOutputDTO modificar(ProfesorInputDTO input, String id) throws Exception;
    public ProfesorOutputDTO findProfesorbyID(String id);
    public Optional<Profesor> findById(String id);
}
