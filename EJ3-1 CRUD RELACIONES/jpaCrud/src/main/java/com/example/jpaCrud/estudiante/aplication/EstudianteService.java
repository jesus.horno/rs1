package com.example.jpaCrud.Estudiante.aplication;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input.AsignarAsignaturasInputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input.CreateEstudianteInputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.CreateEstudianteoOutputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.EstudianteAsignaturasAñadidasOutputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.EstudiantePersonaOutputDTO;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output.EstudianteAsignaturaOutputDTO;

import java.util.ArrayList;
import java.util.Optional;

public interface EstudianteService {
    public CreateEstudianteoOutputDTO añadirEstudiante(CreateEstudianteInputDTO input);
    public String borrarEstudiante(String id);
    public CreateEstudianteoOutputDTO buscarID(String id, String output);
    public ArrayList<CreateEstudianteoOutputDTO> buscarALL();
    public EstudiantePersonaOutputDTO getTotal(String id);
    public CreateEstudianteoOutputDTO getParcial(String id);
    public CreateEstudianteoOutputDTO modificar(CreateEstudianteInputDTO input, String id) throws Exception;
    public CreateEstudianteoOutputDTO findEstudiantebyID(String id);
    public Optional<Estudiante> findById(String id);
    public EstudianteAsignaturasAñadidasOutputDTO AñadirAsignaturas(AsignarAsignaturasInputDTO input, String id);
    public EstudianteAsignaturasAñadidasOutputDTO EliminarAsignaturas(AsignarAsignaturasInputDTO input, String id);
}
