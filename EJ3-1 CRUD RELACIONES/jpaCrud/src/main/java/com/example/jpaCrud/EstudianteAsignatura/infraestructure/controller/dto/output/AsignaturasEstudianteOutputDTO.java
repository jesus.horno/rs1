package com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class AsignaturasEstudianteOutputDTO {
    protected String id_estudiante;
    protected List<EstudianteAsignatura> asignatura;

    public AsignaturasEstudianteOutputDTO(Estudiante estudiante){
        this.id_estudiante = estudiante.getId();
        this.asignatura=estudiante.getAsignaturas();
    }

}
