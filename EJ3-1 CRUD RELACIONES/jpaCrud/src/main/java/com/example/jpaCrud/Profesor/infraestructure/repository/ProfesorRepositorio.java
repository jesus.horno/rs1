package com.example.jpaCrud.Profesor.infraestructure.repository;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.Profesor.domain.Profesor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfesorRepositorio extends JpaRepository<Profesor, String> {
}
