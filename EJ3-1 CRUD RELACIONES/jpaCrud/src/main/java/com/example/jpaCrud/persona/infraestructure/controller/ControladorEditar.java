package com.example.jpaCrud.Persona.infraestructure.controller;

import com.example.jpaCrud.Persona.apllication.PersonaService;
import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorEditar {

    @Autowired
    PersonaService service;

    @PutMapping("/asignacion/{id}")
    public CreatePersonaOutputDTO modificarPersona(@PathVariable int id, @RequestBody Persona p) throws Exception {
        return service.modificar(p,id);
    }

}
