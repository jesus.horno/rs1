package com.example.jpaCrud.Estudiante.domain;

import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input.AsignarAsignaturasInputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input.CreateEstudianteInputDTO;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Profesor.domain.Profesor;
import com.example.jpaCrud.config.generador.StringPrefixedSequenceIdGenerator;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ausencias_seq")
    @GenericGenerator(
            name = "ausencias_seq",
            strategy = "com.example.jpaCrud.config.generador.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "AUS"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%08d")
            })
    @Column(name = "id")
    private String id;

    @OneToOne
    @JoinColumn(name = "fk_idPersona")
    private Persona persona;

    @Column(name="numHours", nullable = false)
    private Integer num_hours_week;

    @ManyToOne
    @JoinColumn(name = "fk_idProfesor")
    private Profesor profesor;

    @OneToMany
    private List<EstudianteAsignatura> asignaturas;

    @Column(name="branch", nullable = false)
    private String branch;

    @Column(name="comments")
    private String coments;

    public Estudiante(CreateEstudianteInputDTO estu, Persona persona, Profesor profesor,List<EstudianteAsignatura> asignatura) {
        if(persona != null)this.persona=persona;
        if(profesor!= null) this.profesor=profesor;
        if(asignatura!=null) this.asignaturas=asignatura;
        if(estu.getNum_hours_week() != null) this.num_hours_week=estu.getNum_hours_week();
        if(estu.getBranch()!=null) this.branch = estu.getBranch();
        if(estu.getComents() !=null) this.coments= estu.getComents();
    }

    public void setAñadirAsignaturas(List<EstudianteAsignatura> listaAñadir){
        System.out.println("TAMAÑOOOOOOOOOOOOOOOOOOO DE LA LISTA:gggggggggggggggggggggggggggggggggg " + listaAñadir.size());
        for(int i=0;i<listaAñadir.size();i++){
            System.out.println("TAMAÑOOOOOOOOOOOOOOOOOOO DE LA LISTA");
            if(!asignaturas.contains(listaAñadir.get(i))){
                asignaturas.add(listaAñadir.get(i));
            }
        }
    }

    public void setEliminarAsignaturas(List<EstudianteAsignatura> listaEliminar){
        for(int i=0;i<listaEliminar.size();i++){
            if(asignaturas.contains(listaEliminar.get(i))){
                asignaturas.remove(listaEliminar.get(i));
            }
        }
    }

}
