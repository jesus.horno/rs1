package com.example.jpaCrud.EstudianteAsignatura.aplication;

import com.example.jpaCrud.Estudiante.aplication.EstudianteService;
import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.EstudiantePersonaOutputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.repository.EstudianteRepositorio;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.input.EstudianteAsignaturaInputDTO;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output.AsignaturasEstudianteOutputDTO;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output.EstudianteAsignaturaOutputDTO;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.repository.EstudianteAsignaturaRepositorio;
import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import com.example.jpaCrud.Persona.infraestructure.repository.PersonaRepositorio;
import com.example.jpaCrud.Profesor.domain.Profesor;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.input.ProfesorInputDTO;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.output.ProfesorOutputDTO;
import com.example.jpaCrud.Profesor.infraestructure.repository.ProfesorRepositorio;
import com.example.jpaCrud.config.error.NotFoundException;
import com.example.jpaCrud.config.error.UnprocesableException;
import com.example.jpaCrud.config.mapper.mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class EstudianteAsignaturaServiceImpl implements EstudianteAsignaturaService {

    @Autowired
    EstudianteAsignaturaRepositorio estudianteAsignaturaRepositorio;

    @Autowired
    EstudianteRepositorio estudianteRepositorio;

    @Autowired
    mapper map;

    public boolean comprobacionNulos(EstudianteAsignatura estudianteAsignatura){
        if(estudianteAsignatura.getInitial_date() == null) {
            System.out.println("es nulo 2");
            return false;
        }
        return true;
    }

    @Override
    public EstudianteAsignaturaOutputDTO añadirEstuadianteAsignatura(EstudianteAsignaturaInputDTO input) {
        Estudiante estudiante = estudianteRepositorio.findById(input.getId_estudiante()).orElseThrow(()-> new NotFoundException("no encontrado"));
        System.out.println("esta es el estudiante " + estudiante);
        EstudianteAsignatura estudianteAsignatura = new EstudianteAsignatura(input,estudiante);
        if(comprobacionNulos(estudianteAsignatura)) {
            estudianteAsignaturaRepositorio.save(estudianteAsignatura);
        } else{
            throw new UnprocesableException("faltan campos");
        }
        EstudianteAsignaturaOutputDTO out= new EstudianteAsignaturaOutputDTO(estudianteAsignatura);
        return out;
    }

    @Override
    public String borrarEstuadianteAsignatura(String id) {
        estudianteAsignaturaRepositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));
        estudianteAsignaturaRepositorio.deleteById(id);

        return "Persona borrada";
    }

    @Override
    public EstudianteAsignaturaOutputDTO buscarID(String id) {
        EstudianteAsignaturaOutputDTO out=new EstudianteAsignaturaOutputDTO(estudianteAsignaturaRepositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado")));
        return out;
    }

    @Override
    public ArrayList<EstudianteAsignaturaOutputDTO> buscarALL() {
        ArrayList<EstudianteAsignaturaOutputDTO> lista = new ArrayList<>();
        for(int i=0;i<estudianteAsignaturaRepositorio.findAll().size();i++){
            System.out.println(estudianteAsignaturaRepositorio.findAll().get(i));
            EstudianteAsignaturaOutputDTO out = new EstudianteAsignaturaOutputDTO(estudianteAsignaturaRepositorio.findAll().get(i));
            lista.add(out);
        }

        return lista;
    }

    @Override
    public EstudianteAsignaturaOutputDTO modificar(EstudianteAsignaturaInputDTO input, String id) throws Exception {
        Estudiante estudiante = estudianteRepositorio.findById(input.getId_estudiante()).orElseThrow(()-> new NotFoundException("no encontrado"));
        System.out.println("esta es la persona " + estudiante);
        EstudianteAsignatura estudianteAsignatura = new EstudianteAsignatura(input,estudiante);
        estudianteAsignatura.setId(id);
        estudianteAsignaturaRepositorio.save(estudianteAsignatura);
        EstudianteAsignaturaOutputDTO out= new EstudianteAsignaturaOutputDTO(estudianteAsignatura);
        return out;
    }

    @Override
    public EstudianteAsignaturaOutputDTO findEstudianteAsignaturabyID(String id) {
        EstudianteAsignatura estudianteAsignatura = estudianteAsignaturaRepositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));

        return map.crearEstudianteAsignaturaOutput(estudianteAsignatura);
    }

    @Override
    public Optional<EstudianteAsignatura> findById(String id) {
        return estudianteAsignaturaRepositorio.findById(id);
    }

    @Override
    public AsignaturasEstudianteOutputDTO buscarIDAsignaturas(String id) {
        Estudiante estudiante = estudianteRepositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));
        AsignaturasEstudianteOutputDTO out = new AsignaturasEstudianteOutputDTO(estudiante);
        return out;
    }


}
