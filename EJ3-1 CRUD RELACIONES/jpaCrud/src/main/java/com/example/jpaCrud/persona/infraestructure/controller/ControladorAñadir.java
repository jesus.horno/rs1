package com.example.jpaCrud.Persona.infraestructure.controller;

import com.example.jpaCrud.Persona.apllication.PersonaService;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.input.CreatePersonaInputDTO;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorAñadir {

    @Autowired
    PersonaService service;

    @PostMapping("/asignacion")
    public CreatePersonaOutputDTO añadirPersona(@RequestBody CreatePersonaInputDTO createPersonalInputDTO) {
        CreatePersonaOutputDTO out=service.añadir(service.dtoEntrada(createPersonalInputDTO));
        return out;
    }

}
