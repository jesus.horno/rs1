package com.example.jpaCrud.Persona.apllication;

import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.input.CreatePersonaInputDTO;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.output.ProfesorOutputDTO;

import java.util.ArrayList;
import java.util.Optional;

public interface PersonaService {

    public Persona dtoEntrada(CreatePersonaInputDTO input);
    public CreatePersonaOutputDTO añadir(Persona p);
    public boolean comprobacionNulos(Persona perso);
    public CreatePersonaOutputDTO modificar(Persona p, int id) throws Exception;
    public void borrar(int id) throws Exception;
    public CreatePersonaOutputDTO buscarID(int id) throws Exception;
    public ArrayList<CreatePersonaOutputDTO> buscarALL();
    public ArrayList<CreatePersonaOutputDTO> buscarNombre(String nombre);
    public CreatePersonaOutputDTO findPersonabyID(int id);
    public Optional<Persona> findById(int id);
}
