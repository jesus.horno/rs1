package com.example.jpaCrud.Persona.infraestructure.controller.dto.output;

import com.example.jpaCrud.Persona.domain.Persona;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CreatePersonaOutputDTO implements Serializable {
    private int id_persona;
    private String usuario;
    private String password;
    private String name;
    private String surname;
    private String company_email;
    private String personal_email;
    private String city;
    private Boolean active;
    private Date created_date;
    private String imagen_url;
    private Date termination_date;

    //Se va a acceder al output a traves del servicio
    public CreatePersonaOutputDTO personaOutputDTO(Persona p){
//        if(p == null) return null;
//        p.setId_persona(this.getId_persona());

        CreatePersonaOutputDTO out=new CreatePersonaOutputDTO();
        out.setId_persona(p.getId_persona());
        out.setUsuario(p.getUsuario());
        out.setPassword(p.getPassword());
        out.setName(p.getName());
        out.setSurname(p.getSurname());
        out.setCompany_email(p.getCompany_email());
        out.setPersonal_email(p.getPersonal_email());
        out.setCity(p.getCity());
        out.setActive(p.getActive());
        out.setCreated_date(p.getCreated_date());
        out.setImagen_url(p.getImagen_url());
        out.setTermination_date(p.getTermination_date());
        return out;
    }
}
