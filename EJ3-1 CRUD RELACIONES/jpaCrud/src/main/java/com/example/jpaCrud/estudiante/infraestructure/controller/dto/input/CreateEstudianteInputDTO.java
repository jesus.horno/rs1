package com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input;

import lombok.Data;

@Data
public class CreateEstudianteInputDTO {
    private int id_persona;
    private Integer num_hours_week;
    private String[] id_asignaturas;
    private String coments;
    private String id_profesor;
    private String branch;
}
