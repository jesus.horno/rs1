package com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input;

import lombok.Data;

@Data
public class AsignarAsignaturasInputDTO {

    private String[] id_asignaturas;
}
