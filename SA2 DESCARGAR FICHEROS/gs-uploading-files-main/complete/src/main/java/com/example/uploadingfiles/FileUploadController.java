package com.example.uploadingfiles;

import com.example.uploadingfiles.storage.FileSystemStorageService;
import com.example.uploadingfiles.storage.StorageFileNotFoundException;
import com.example.uploadingfiles.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

@RestController
public class FileUploadController {

	private final StorageService storageService;

	@Autowired
	FileSystemStorageService filesystem;

	@Autowired
	public FileUploadController(StorageService storageService) {
		this.storageService = storageService;
	}

	@GetMapping("/ruta")
	public String listUploadedFilesRuta(@RequestParam(value = "path") String path) throws IOException {
		filesystem.setPath(path);
		return "Se a establecido el directorio de descarga";
	}

	@PostMapping("/upload/{tipo}")
	public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file,
												   RedirectAttributes redirectAttributes, @PathVariable String tipo) {
		String extension = org.apache.commons.io.FilenameUtils.getExtension(file.getOriginalFilename());
		if (extension.equals(tipo)) {
			storageService.store(file);
			return ResponseEntity.ok("El fichero ha sido subido correctamente");
		} else {
			return new ResponseEntity<>(
					"La extenxion del fichero no es la correcta" +
							file.getName(), HttpStatus.BAD_REQUEST);
		}
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

}
