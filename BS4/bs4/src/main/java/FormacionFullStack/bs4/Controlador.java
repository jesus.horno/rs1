package FormacionFullStack.bs4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {

//    @Value("${var1}")
//    private String valor;
//    @Value("${My.var2}")
//    private String var2;
    @Value("${path:var3 no teine valor todavia}")//DUDOSO PREGUNTAR SOBRE VARIABLES DEL SISTEMA https://www.java.com/es/download/help/path_es.html RUTA PAGINA ARCHIVO
    private String var3;

    @Autowired
    Variables v;
    @Autowired
    Configuracion conf;
    @Autowired
    Perfiles perfil;

    @GetMapping("/valores")
    public String devolverValores(){
        return "Valor de var1 es: " + v.getVar1() + "El valor de la segunda variable es: " + v.getVar2() + "El valor de la tercera variable es: " + var3;
    }

    @GetMapping("/parametros")
    public String devolverParametros(){
        return "la url es: " + v.getVar1() + " la pass es: " + v.getVar2();
    }

    @GetMapping("/miconfiguracion")
    public String devolverParametrosConfiguracion(){
        return "valor 1 es: " + conf.getValor1() + " valor2 es "+ conf.getValor2();
    }

    @GetMapping("/perfil")
    public void devolverPerfil(){
        perfil.mifuncion() ;
    }
}
