package FormacionFullStack.bs4;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@Profile("perfil1")
public class perfil1 implements Perfiles{

    private String perfil="perfil 1";

    @Override
    public void mifuncion() {
        System.out.println(perfil);
    }



}
