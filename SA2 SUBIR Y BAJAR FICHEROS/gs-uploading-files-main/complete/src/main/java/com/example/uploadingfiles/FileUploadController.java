package com.example.uploadingfiles;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import com.example.uploadingfiles.storage.FileSystemStorageService;
import com.example.uploadingfiles.storage.StorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.uploadingfiles.storage.StorageFileNotFoundException;
import com.example.uploadingfiles.storage.StorageService;

@RestController
public class FileUploadController {

	private final StorageService storageService;

	@Autowired
	FileSystemStorageService filesystem;

	@Autowired
	public FileUploadController(StorageService storageService) {
		this.storageService = storageService;
	}

	@GetMapping("/")
	public String listUploadedFiles(Model model) throws IOException {

		model.addAttribute("files", storageService.loadAll().map(
				path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
						"serveFile", path.getFileName().toString()).build().toUri().toString())
				.collect(Collectors.toList()));

		return "uploadForm";
	}


	@GetMapping("/ruta")
	public String listUploadedFilesRuta(@RequestParam(value="path") String path) throws IOException {
		System.out.println(path);
		filesystem.setPath(path);

		return path;
	}

	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}

	@PostMapping("/")
	public String handleFileUpload(@RequestParam("file") MultipartFile file,
			RedirectAttributes redirectAttributes) {


		String extension=org.apache.commons.io.FilenameUtils.getExtension(file.getOriginalFilename());
		System.out.println(extension);
				storageService.store(file);
		redirectAttributes.addFlashAttribute("message",
				"You successfully uploaded " + file.getOriginalFilename() + "!");

		return "redirect:/";
	}

	@PostMapping("/upload/{tipo}")
	public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file,
								   RedirectAttributes redirectAttributes, @PathVariable String tipo) {


		String extension=org.apache.commons.io.FilenameUtils.getExtension(file.getOriginalFilename());
		if(extension.equals(tipo)){
			storageService.store(file);
			return new ResponseEntity<>("El fichero ha sido subido correctamente" , HttpStatus.OK);}
		else{
			return new ResponseEntity<>(
					"La extenxion del fichero no es la correcta" +
									file.getOriginalFilename(), HttpStatus.BAD_REQUEST);
			}
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

}
