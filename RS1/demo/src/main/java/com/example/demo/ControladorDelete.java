package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorDelete {

    @Autowired
    PersonaService person;

    @DeleteMapping("/controladorD/persona/{id}")
    public void deletePersonaID(@PathVariable int id){
        person.getPersonas().remove(id);
    }

}
