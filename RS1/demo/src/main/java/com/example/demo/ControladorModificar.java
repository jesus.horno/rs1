package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorModificar {

    @Autowired
    PersonaService person;

    @PutMapping("/controladorM/persona/{id}")
    public Persona modificarPersona(@PathVariable int id, @RequestBody Persona p){
        for(int i =0 ;i<person.getPersonas().size();i++){
            if(id == i){
                Persona paux = new Persona();
                if(p.getEdad() != null) {
                    paux.setEdad(p.getEdad());
                }

                if(p.getNombre() != null){
                    paux.setNombre(p.getNombre());
                }

                if(p.getPoblacion() != null){
                    paux.setPoblacion(p.getPoblacion());
                }

                person.getPersonas().remove(i);
                person.addPersonasIndex(paux, i);
                return person.getPersonas().get(i);
            }
        }
        Persona p1 = new Persona();
        return p1;
    }

}
