package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorGET {

    @Autowired
    PersonaService persona;

    @GetMapping("controlador/persona/{id}")
    public Persona getPersonaID(@PathVariable int id){
        for(int i =0 ;i<persona.getPersonas().size();i++){
            if(id == i){
                return persona.getPersonas().get(i);
            }
        }
        Persona p = new Persona();
        return p;
    }

    @GetMapping("/controlador/persona/{nombre}")
    public Persona getPersonaNom(@PathVariable String nombre){
        for(int i =0 ;i<persona.getPersonas().size();i++){
            if(persona.getPersonas().get(i).getNombre().equals(nombre)){
                return persona.getPersonas().get(i);
            }
        }
        Persona p = new Persona();
        return p;
    }

}
