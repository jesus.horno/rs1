package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service //en la clase no en la interfaz
public class PersonaServiceImpl implements PersonaService{

    Persona p = new Persona();
    ArrayList<Persona> personas = new ArrayList<>();

    @Override
    public void setNombre(String nombre) {
        p.setNombre(nombre);
    }
    @Override
    public void setPoblacion(String poblacion) {
        p.setPoblacion(poblacion);
    }
    @Override
    public void setEdad(int edad) {
        p.setEdad(edad);
    }
    @Override
    public String getNombre() {
        return p.getNombre();
    }
    @Override
    public String getPoblacion() {
        return p.getPoblacion();
    }
    @Override
    public int getEdad() {
        return p.getEdad();
    }
    @Override
    public Persona getPersona() {
        return p;
    }

    @Override
    public ArrayList<Persona> getPersonas() {
        return personas;
    }

    @Override
    public void addPersonas(Persona per) {
        personas.add(per);
    }

    @Override
    public void addPersonasIndex(Persona per, int pos) {
        personas.add(pos, per);
    }


//    @Override
//    public Persona getPersonaBeen(String key) {
//        switch(key){
//            case "been1":
//                Persona p1 = new Persona("Pepe","Alcaudete",20);
//                return p1;
//            case "been2":
//                Persona p2 = new Persona("Kike","Murcia",23);
//                return p2;
//            case "been3":
//                Persona p3 = new Persona("Julian","Snatander",24);
//                return p3;
//        }
//        Persona p4 = new Persona("","",0);
//        return p4;
//    }

}
