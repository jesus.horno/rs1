package com.example.demo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;


public interface PersonaService {
    void setNombre(String nombre);
    void setPoblacion(String poblacion);
    void setEdad(int edad);
    String getNombre();
    String getPoblacion();
    int getEdad();
    Persona getPersona();
    public ArrayList<Persona> getPersonas();
    public void addPersonas(Persona per);
    public void addPersonasIndex(Persona per, int pos);
}
