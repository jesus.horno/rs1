package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class Controlador {

    @Autowired
    PersonaService persona;

    @GetMapping("/ab")
    public String HelloWorld(){
        return "calvo";
    }

    @GetMapping("/user/{nombre}")
    public String HelloWorld1(@PathVariable String nombre){
        return "hola " + nombre;
    }

    /**
     * FUNCION PARA RECIBIR PETICIONES POST EN ESA URL
     * @param nombre PARAMETROS PASADOS EN LA PETICION POS
     * @return LOS PARAMETROS PASADOS PERO SUMANDO UN AÑO A LA EDAD
     */
    @PostMapping("/post")
    public Persona postBody(@RequestBody Persona nombre){

        //Persona p = new Persona();
        //p.setNombre(nombre.getNombre());
        nombre.setEdad(nombre.getEdad()+1); //SUPUESTAMENTE NO HACE FALTA HACER LOS SET POR QUE JSON LO HACEN INTERNAMENTE
       // p.setPoblacion(nombre.getPoblacion());

        return nombre;
    }

    @PostMapping("/persona")
    public ArrayList<Persona> postPersonas (@RequestBody Persona per){
        persona.addPersonas(per);
        return persona.getPersonas();
    }

}
