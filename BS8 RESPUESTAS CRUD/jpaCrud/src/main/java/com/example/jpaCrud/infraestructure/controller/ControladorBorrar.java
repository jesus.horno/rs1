package com.example.jpaCrud.infraestructure.controller;

import com.example.jpaCrud.apllication.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorBorrar {

    @Autowired
    PersonaService service;

    @DeleteMapping("/asignacion/{id}")
    public String deletePersona(@PathVariable int id) throws Exception {
        service.borrar(id);
        return "persona deleteada";
    }

}
