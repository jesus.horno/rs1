package FormacionFullStack.bs3;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Tercero implements CommandLineRunner {
    
    @Override
    public void run(String... args) throws Exception {
        System.out.println("tercera linea");
    }
}
