package FormacionFullStack.bs3;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Primero {

    @PostConstruct
    public void ejecutate(){
        System.out.println("Hola desde clase inicial");
    }

}
