package com.example.jpaCrud.Profesor.infraestructure.controller.dto.output;

import com.example.jpaCrud.Profesor.domain.Profesor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ProfesorOutputDTO {
    protected String id;
    protected int id_persona;
    protected List<String> id_estudiante = new ArrayList<>();
    protected String coments;
    protected String branch;

    public ProfesorOutputDTO(Profesor profesor){
        this.id=profesor.getId();
        this.id_persona= profesor.getPersona().getId_persona();
        for(int i=0;i<profesor.getListaEstudiantes().size();i++){
            this.id_estudiante.add(profesor.getListaEstudiantes().get(i).getId());
        }
        this.coments=profesor.getComents();
        this.branch=profesor.getBranch();
    }
}
