package com.example.jpaCrud.Estudiante.infraestructure.repository;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstudianteRepositorio extends JpaRepository<Estudiante, String> {
}
