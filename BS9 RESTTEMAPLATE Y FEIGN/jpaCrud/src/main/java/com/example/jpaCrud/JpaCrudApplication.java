package com.example.jpaCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@EnableFeignClients
public class JpaCrudApplication {


	public static void main(String[] args) {
		SpringApplication.run(JpaCrudApplication.class, args);
	}


}
