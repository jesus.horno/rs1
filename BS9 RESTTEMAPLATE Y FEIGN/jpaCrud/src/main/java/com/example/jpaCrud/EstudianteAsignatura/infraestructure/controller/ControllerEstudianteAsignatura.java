package com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller;


import com.example.jpaCrud.EstudianteAsignatura.aplication.EstudianteAsignaturaService;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.input.EstudianteAsignaturaInputDTO;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output.AsignaturasEstudianteOutputDTO;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output.EstudianteAsignaturaOutputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("asignatura")
public class ControllerEstudianteAsignatura {

    @Autowired
    EstudianteAsignaturaService service;

    @PostMapping("/asignacion")
    public EstudianteAsignaturaOutputDTO añadirPersona(@RequestBody EstudianteAsignaturaInputDTO estudianteAsignaturaInputDTO) {
        return service.añadirEstuadianteAsignatura(estudianteAsignaturaInputDTO);
    }

    @DeleteMapping("/borrado/{id}")
    public String DeleteEstudianteAsignatura(@PathVariable String id) throws Exception {
        return service.borrarEstuadianteAsignatura(id);
    }

    @GetMapping("/asignacion")
    public ArrayList<EstudianteAsignaturaOutputDTO> getPersona() throws Exception {
        return service.buscarALL();
    }

    @GetMapping("/asignacion/{id}")
    public EstudianteAsignaturaOutputDTO getEstudianteAsignatura(@PathVariable String id) throws Exception {
        return service.buscarID(id);
    }

    @PutMapping("/asignacion/{id}")
    public EstudianteAsignaturaOutputDTO modificarEstudianteAsignatura(@PathVariable String id, @RequestBody EstudianteAsignaturaInputDTO input) throws Exception {
        return service.modificar(input,id);
    }

    @GetMapping("/asignaturas/{id}")
    public AsignaturasEstudianteOutputDTO getAsignaturasEstudiante(@PathVariable String id) throws Exception {
        return service.buscarIDAsignaturas(id);
    }
}
