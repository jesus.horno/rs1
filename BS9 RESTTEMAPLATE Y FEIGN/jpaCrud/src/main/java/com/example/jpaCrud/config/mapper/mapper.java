package com.example.jpaCrud.config.mapper;

import com.example.jpaCrud.Estudiante.aplication.EstudianteService;
import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.CreateEstudianteoOutputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.EstudiantePersonaOutputDTO;
import com.example.jpaCrud.EstudianteAsignatura.aplication.EstudianteAsignaturaService;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output.EstudianteAsignaturaOutputDTO;
import com.example.jpaCrud.Persona.apllication.PersonaService;
import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.input.CreatePersonaInputDTO;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import com.example.jpaCrud.Profesor.aplication.ProfesorService;
import com.example.jpaCrud.Profesor.domain.Profesor;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.input.ProfesorInputDTO;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.output.ProfesorOutputDTO;
import com.example.jpaCrud.config.error.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class mapper {
    @Autowired
    EstudianteService estudianteServicio;
    @Autowired
    PersonaService personaService;
    @Autowired
    ProfesorService profesorService;
    @Autowired
    EstudianteAsignaturaService estudianteAsignaturaService;

    public Profesor crearProfesor(ProfesorInputDTO input){
        Profesor profesor = new Profesor();
        profesor.setBranch(input.getBranch());
        profesor.setComents(input.getComents());
        profesor.setPersona(personaService.findById(input.getId_persona()).get());
        //falta lista de estudiantes
        List<Estudiante> listaAux = new ArrayList<>();
        for(int i=0;i<input.getId_estudiante().length;i++){
            Estudiante estudiante = estudianteServicio.findById(input.getId_estudiante()[i]).orElseThrow(()-> new NotFoundException("no encontrado"));
            listaAux.add(estudiante);
        }
        profesor.setListaEstudiantes(listaAux);
        return profesor;
    }

    public Persona crearPersona(CreatePersonaInputDTO input){
        Persona persona = new Persona();

        persona.setUsuario(input.getUsuario());
        persona.setPassword(input.getPassword());
        persona.setName(input.getName());
        persona.setSurname(input.getSurname());
        persona.setCompany_email(input.getCompany_email());
        persona.setPersonal_email(input.getPersonal_email());
        persona.setCity(input.getCity());
        persona.setActive(input.getActive());
        persona.setCreated_date(input.getCreated_date());
        persona.setImagen_url(input.getImagen_url());
        persona.setTermination_date(input.getTermination_date());
        return persona;

    }

    public ProfesorOutputDTO crearProfesorOutput(Profesor profesor){
        ProfesorOutputDTO out = new ProfesorOutputDTO();
        out.setBranch(profesor.getBranch());
        out.setComents(profesor.getComents());
        out.setId(profesor.getId());
        //falta los estudiantes
        return out;
    }

    public CreatePersonaOutputDTO crearPersonaOutput(Persona persona){
        CreatePersonaOutputDTO out=new CreatePersonaOutputDTO();
        out.setId_persona(persona.getId_persona());
        out.setUsuario(persona.getUsuario());
        out.setPassword(persona.getPassword());
        out.setName(persona.getName());
        out.setSurname(persona.getSurname());
        out.setCompany_email(persona.getCompany_email());
        out.setPersonal_email(persona.getPersonal_email());
        out.setCity(persona.getCity());
        out.setActive(persona.getActive());
        out.setCreated_date(persona.getCreated_date());
        out.setImagen_url(persona.getImagen_url());
        out.setTermination_date(persona.getTermination_date());
        return out;
    }

    public EstudianteAsignaturaOutputDTO crearEstudianteAsignaturaOutput(EstudianteAsignatura estudianteAsignatura){
        EstudianteAsignaturaOutputDTO out = new EstudianteAsignaturaOutputDTO();
        out.setId_estudiante(estudianteAsignatura.getId());
        out.setId_estudiante(estudianteAsignatura.getEstudianteCursandola().getId());
        out.setAsignatura(estudianteAsignatura.getAsignatura());
        out.setInitial_date(estudianteAsignatura.getInitial_date());
        out.setFinish_date(estudianteAsignatura.getFinish_date());
        out.setComents(estudianteAsignatura.getComents());
        return out;
    }

    public CreateEstudianteoOutputDTO crearEstudianteOutput(Estudiante estudiante){
        CreateEstudianteoOutputDTO out = new CreateEstudianteoOutputDTO();
        out.setId(estudiante.getId());
        out.setId_persona(estudiante.getPersona().getId_persona());
        out.setId_profesor(estudiante.getProfesor().getId());
        out.setComents(estudiante.getComents());
        out.setNum_hours_week(estudiante.getNum_hours_week());
        out.setBranch(estudiante.getBranch());
        return out;
    }

    public EstudiantePersonaOutputDTO crearEstudianteOutput2(Estudiante estudiante){
        EstudiantePersonaOutputDTO out = new EstudiantePersonaOutputDTO();
        //out.setUser();
        return out;
    }


}
