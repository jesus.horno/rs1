package com.example.jpaCrud.Persona.infraestructure.controller;

import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Persona.infraestructure.repository.PersonaRepositorio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class Controlador {

    @Autowired
    PersonaRepositorio personRepo;

    public boolean comprobacionNulos(Persona perso){
        if(perso.getUsuario() ==null || perso.getPassword()==null || perso.getName() ==null || perso.getCompany_email() ==null || perso.getPersonal_email() == null || perso.getCity()==null || perso.getCreated_date()==null) {
            System.out.println("es nulo 2");
            return false;
        }
        return true;
    }

    @GetMapping("/id/{id}")
    public Persona getPersona(@PathVariable int id) throws Exception {
        return personRepo.findById(id).orElseThrow(()-> new Exception("no encontrado"));
    }

    @GetMapping("/nombre/{nombre}")
    public ArrayList<Persona> getPersona(@PathVariable String nombre){
        return personRepo.findByUsuario(nombre);
    }

    @PutMapping("/{id}")
    public Persona modificarPersona(@PathVariable int id, @RequestBody Persona p) throws Exception {
        personRepo.findById(id).orElseThrow(()-> new Exception("no encontrado"));
        p.setId_persona(id);
        personRepo.save(p);
        return p;
    }

    @DeleteMapping("/id/{id}")
    public String deletePersona(@PathVariable int id) throws Exception {
            personRepo.findById(id).orElseThrow(()-> new Exception("no encontrado"));
            personRepo.deleteById(id);
            return "persona deleteada";

    }

    @PostMapping
    public String añadirPersona(@RequestBody Persona p) {
        System.out.println("hola");
        if (comprobacionNulos(p)) {
            System.out.println("es nulo 3");
            personRepo.save(p);
            return "Persona añadida";
        } else {
            return "No se ha podido añadir a la persona";

        }
    }


}
