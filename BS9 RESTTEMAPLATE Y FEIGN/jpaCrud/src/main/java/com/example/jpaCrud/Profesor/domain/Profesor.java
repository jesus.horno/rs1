package com.example.jpaCrud.Profesor.domain;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.input.ProfesorInputDTO;
import com.example.jpaCrud.config.generador.StringPrefixedSequenceIdGenerator;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Profesor {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ausencias_seq")
    @GenericGenerator(
            name = "ausencias_seq",
            strategy = "com.example.jpaCrud.config.generador.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "AUS"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%07d")
            })
    @Column(name = "id")
    private String id;

    @OneToOne
    @JoinColumn(name = "fk_idPersona")
    private Persona persona;

    @Column(name="coments")
    private String coments;

    @Column(name="branch", nullable = false)
    private String branch;

    @OneToMany
//    @JoinColumn(name = "fk_idEstudiantes")
    private List<Estudiante> listaEstudiantes;

    public Profesor(ProfesorInputDTO input, Persona persona,List<Estudiante> estudiante ){
        if(persona!=null) this.persona = persona;
        if(estudiante!=null) this.listaEstudiantes=estudiante;
        if(input.getComents()!=null) this.coments = input.getComents();
        if(input.getBranch()!=null) this.branch= input.getBranch();
    }


}
