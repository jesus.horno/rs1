package com.example.jpaCrud.EstudianteAsignatura.aplication;

import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.EstudiantePersonaOutputDTO;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.input.EstudianteAsignaturaInputDTO;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output.AsignaturasEstudianteOutputDTO;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output.EstudianteAsignaturaOutputDTO;
import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.input.ProfesorInputDTO;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.output.ProfesorOutputDTO;

import java.util.ArrayList;
import java.util.Optional;

public interface EstudianteAsignaturaService {

    public EstudianteAsignaturaOutputDTO añadirEstuadianteAsignatura(EstudianteAsignaturaInputDTO input);
    public String borrarEstuadianteAsignatura(String id);
    public EstudianteAsignaturaOutputDTO buscarID(String id);
    public ArrayList<EstudianteAsignaturaOutputDTO> buscarALL();
    public EstudianteAsignaturaOutputDTO modificar(EstudianteAsignaturaInputDTO input, String id) throws Exception;
    public EstudianteAsignaturaOutputDTO findEstudianteAsignaturabyID(String id);
    public Optional<EstudianteAsignatura> findById(String id);
    public AsignaturasEstudianteOutputDTO buscarIDAsignaturas(String id);
}
