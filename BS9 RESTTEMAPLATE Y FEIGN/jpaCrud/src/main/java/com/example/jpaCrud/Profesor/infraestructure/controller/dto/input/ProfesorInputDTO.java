package com.example.jpaCrud.Profesor.infraestructure.controller.dto.input;

import lombok.Data;

@Data
public class ProfesorInputDTO {

    private int id_persona;
    private String[] id_estudiante;
    private String coments;
    private String branch;
}
