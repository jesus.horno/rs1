package com.example.jpaCrud.Persona.apllication;

import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.input.CreatePersonaInputDTO;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import com.example.jpaCrud.Persona.infraestructure.repository.PersonaRepositorio;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.output.ProfesorOutputDTO;
import com.example.jpaCrud.config.error.NotFoundException;
import com.example.jpaCrud.config.error.UnprocesableException;
import com.example.jpaCrud.config.mapper.mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class PersonaServiceimpl implements PersonaService{

    @Autowired
    PersonaRepositorio person;

    @Autowired
    mapper map;

    @Override
    public Persona dtoEntrada(CreatePersonaInputDTO input) {

        return input.personaDTO(input);
    }

    public boolean comprobacionNulos(Persona perso){
        if(perso.getUsuario() ==null || perso.getPassword()==null || perso.getName() ==null || perso.getCompany_email() ==null || perso.getPersonal_email() == null || perso.getCity()==null || perso.getCreated_date()==null) {
            System.out.println("es nulo 2");
            return false;
        }
        return true;
    }

    public CreatePersonaOutputDTO añadir(Persona p) {
        CreatePersonaOutputDTO out=new CreatePersonaOutputDTO();
        if(comprobacionNulos(p)){
            out.personaOutputDTO(p);
            person.save(p);
            return out.personaOutputDTO(p);
        }else{
            throw new UnprocesableException("faltan campos");
        }
    }

    public CreatePersonaOutputDTO modificar(Persona p, int id) throws Exception {
        person.findById(id).orElseThrow(()-> new UnprocesableException("id no encontrada"));
        p.setId_persona(id);
        person.save(p);
        CreatePersonaOutputDTO out = new CreatePersonaOutputDTO();
        return out.personaOutputDTO(p);
    }

    public void borrar(int id) throws Exception {
        person.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));
        person.deleteById(id);
    }

    public CreatePersonaOutputDTO buscarID(int id) throws Exception {
        CreatePersonaOutputDTO out = new CreatePersonaOutputDTO();
        return out.personaOutputDTO(person.findById(id).orElseThrow(()-> new NotFoundException("no encontrado")));
    }

    public ArrayList<CreatePersonaOutputDTO> buscarALL() {
        CreatePersonaOutputDTO out = new CreatePersonaOutputDTO();
        ArrayList<CreatePersonaOutputDTO> lista = new ArrayList<>();
        for(int i=0;i<person.findAll().size();i++){
            lista.add(out.personaOutputDTO(person.findAll().get(i)));
        }

        return lista;
    }

    public ArrayList<CreatePersonaOutputDTO> buscarNombre(String nombre) {
        CreatePersonaOutputDTO out = new CreatePersonaOutputDTO();
        ArrayList<CreatePersonaOutputDTO> lista = new ArrayList<>();
        for(int i=0;i<person.findByUsuario(nombre).size();i++){
            lista.add(out.personaOutputDTO(person.findByUsuario(nombre).get(i)));
        }

        return lista;
    }

    @Override
    public CreatePersonaOutputDTO findPersonabyID(int id) {
        Persona persona = person.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));

        return map.crearPersonaOutput(persona);
    }

    @Override
    public Optional<Persona> findById(int id) {
        return person.findById(id);
    }



}
