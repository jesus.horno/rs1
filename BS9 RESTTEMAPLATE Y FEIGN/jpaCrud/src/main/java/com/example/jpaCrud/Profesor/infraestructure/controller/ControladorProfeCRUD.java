package com.example.jpaCrud.Profesor.infraestructure.controller;

import com.example.jpaCrud.Estudiante.aplication.EstudianteService;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input.CreateEstudianteInputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.CreateEstudianteoOutputDTO;
import com.example.jpaCrud.Profesor.aplication.ProfesorService;
import com.example.jpaCrud.Profesor.domain.Profesor;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.input.ProfesorInputDTO;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.output.ProfesorOutputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@RestController
@RequestMapping("profesor")
public class ControladorProfeCRUD {
    @Autowired
    ProfesorService service;

    @PostMapping("/asignacion")
    public ProfesorOutputDTO añadirPersona(@RequestBody ProfesorInputDTO profesorInputDTO) {
        return service.añadirProfesor(profesorInputDTO);
    }

    @DeleteMapping("/borrado/{id}")
    public String DeletePersona(@PathVariable String id) throws Exception {
        return service.borrarProfesor(id);
    }

    @GetMapping("/asignacion")
    public ArrayList<ProfesorOutputDTO> getPersona() throws Exception {
        return service.buscarALL();
    }

    @GetMapping("/asignacion/{id}")
    public ProfesorOutputDTO getPersona(@PathVariable String id) throws Exception {
        return service.buscarID(id);
    }

    @PutMapping("/asignacion/{id}")
    public ProfesorOutputDTO modificarPersona(@PathVariable String id, @RequestBody ProfesorInputDTO input) throws Exception {
        return service.modificar(input,id);
    }


}
