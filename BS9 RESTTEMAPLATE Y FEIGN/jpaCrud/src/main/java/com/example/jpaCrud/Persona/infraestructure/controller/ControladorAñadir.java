package com.example.jpaCrud.Persona.infraestructure.controller;

import com.example.jpaCrud.Persona.apllication.PersonaService;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.input.CreatePersonaInputDTO;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import com.example.jpaCrud.Profesor.domain.Profesor;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.output.ProfesorOutputDTO;
import com.example.jpaCrud.config.feign.IFeignServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class ControladorAñadir {

    @Autowired
    PersonaService service;

    @PostMapping("/asignacion")
    public CreatePersonaOutputDTO añadirPersona(@RequestBody CreatePersonaInputDTO createPersonalInputDTO) {
        CreatePersonaOutputDTO out=service.añadir(service.dtoEntrada(createPersonalInputDTO));

        return out;
    }

//    @GetMapping("/profesor/{id}")
//    public ProfesorOutputDTO getProfesor(@PathVariable String id){
//        ResponseEntity<ProfesorOutputDTO> responseEntity= new RestTemplate().getForEntity("http://localhost:8080/profesor/asignacion/"+id, ProfesorOutputDTO.class);
//        System.out.println("SALE EL BODY DEL RESPONSE: " + responseEntity.getBody());
//        return responseEntity.getBody();
//    }


    @Autowired
    IFeignServer feign;

    @GetMapping("/profesor/{id}")
    ResponseEntity<ProfesorOutputDTO> callUsingFeign(@PathVariable String id){
        System.out.println("En client. Antes de llamada a server Devolvere: " + id);
        ResponseEntity<ProfesorOutputDTO> respuesta = feign.callServer(id);
        System.out.println("En client. Despues de llamada a server");
        return respuesta;
    }

    @GetMapping("/template/{id}")
    ResponseEntity<ProfesorOutputDTO> callUsingRestTemplate(@PathVariable String id){
        System.out.println("En client RestTemplate. Antes de llamada a server Devolvere: " + id);
        ResponseEntity<ProfesorOutputDTO> rs = new RestTemplate().getForEntity("http://localhost:8080/profesor/asignacion/"+id, ProfesorOutputDTO.class);
        System.out.println("En client RestTemplate. Despues de llamada a server");
        return rs;
    }

}
