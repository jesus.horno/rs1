package com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;

@Data
@NoArgsConstructor
public class EstudiantePersonaOutputDTO extends CreateEstudianteoOutputDTO{

//    private String id;
//    private Integer num_hours_week;
//    private int id_persona;
//    private String coments;
    //    private Profesor id_profesor;
    private String user;
    private String password;
    private String name;
    private String surname;
    private String company_email;
    private String personal_email;
    private String city;
    private Boolean active;
    private Date created_date;
    private String imagen_url;
    private Date termination_date;

    public EstudiantePersonaOutputDTO(Estudiante estudiante){
        this.id=estudiante.getId();
        this.id_persona= estudiante.getPersona().getId_persona();
        this.coments=estudiante.getComents();
        this.branch=estudiante.getBranch();
        this.id_profesor=estudiante.getProfesor().getId();
        this.num_hours_week = estudiante.getNum_hours_week();
        this.user=estudiante.getPersona().getUsuario();
        this.password = estudiante.getPersona().getPassword();
        this.name=estudiante.getPersona().getName();
        this.surname=estudiante.getPersona().getSurname();
        this.company_email = estudiante.getPersona().getCompany_email();
        this.personal_email=estudiante.getPersona().getPersonal_email();
        this.city=estudiante.getPersona().getCity();
        this.active=estudiante.getPersona().getActive();
        this.created_date=estudiante.getPersona().getCreated_date();
        this.imagen_url=estudiante.getPersona().getImagen_url();
        this.termination_date=estudiante.getPersona().getTermination_date();
    }




}
