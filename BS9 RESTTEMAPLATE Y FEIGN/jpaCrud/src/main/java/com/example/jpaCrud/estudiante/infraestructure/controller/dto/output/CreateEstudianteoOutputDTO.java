package com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateEstudianteoOutputDTO {
    protected String id;
    protected int id_persona;
    protected Integer num_hours_week;
    protected String coments;
    protected String id_profesor;
    protected String branch;

    public CreateEstudianteoOutputDTO(Estudiante estudiante){
        this.id=estudiante.getId();
        this.id_persona= estudiante.getPersona().getId_persona();
        this.id_profesor = estudiante.getProfesor().getId();
        this.coments=estudiante.getComents();
        this.num_hours_week = estudiante.getNum_hours_week();
        this.branch=estudiante.getBranch();
    }

}
