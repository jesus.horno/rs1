package com.example.jpaCrud.Profesor.aplication;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.Estudiante.infraestructure.repository.EstudianteRepositorio;
import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Persona.infraestructure.repository.PersonaRepositorio;
import com.example.jpaCrud.Profesor.domain.Profesor;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.input.ProfesorInputDTO;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.output.ProfesorOutputDTO;
import com.example.jpaCrud.Profesor.infraestructure.repository.ProfesorRepositorio;
import com.example.jpaCrud.config.error.NotFoundException;
import com.example.jpaCrud.config.error.UnprocesableException;
import com.example.jpaCrud.config.mapper.mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class profesorServiceImpl implements ProfesorService{

    @Autowired
    ProfesorRepositorio profesorRepositorio;

    @Autowired
    PersonaRepositorio personaRepositorio;

    @Autowired
    EstudianteRepositorio estudianteRepositorio;

    @Autowired
    mapper mapper;


    public boolean comprobacionNulos(Profesor profesor){
        if(profesor.getBranch() == null) {
            System.out.println("es nulo 2");
            return false;
        }
        return true;
    }

    @Override
    public ProfesorOutputDTO añadirProfesor(ProfesorInputDTO input) {
        Persona persona = personaRepositorio.findById(input.getId_persona()).orElseThrow(()-> new NotFoundException("no encontrado"));
        System.out.println("esta es la persona " + persona);
        List<Estudiante> listaEstudiantes= new ArrayList<>();
        for(int i=0;i<input.getId_estudiante().length;i++){
            Estudiante estudiante = estudianteRepositorio.findById(input.getId_estudiante()[i]).orElseThrow(()-> new NotFoundException("no encontrado"));
            listaEstudiantes.add(estudiante);
        }
        //Estudiante estudiante = estudianteRepositorio.findById(input.getId_estudiante()).orElseThrow(()-> new NotFoundException("no encontrado"));

        Profesor profesor = new Profesor(input,persona, listaEstudiantes);
        System.out.println("ESTE ES EL PROFESOR:    " + profesor);
        if(comprobacionNulos(profesor)) {

            System.out.println("ESTE ES EL PROFESOR2 :    " + profesor);
            profesorRepositorio.save(profesor);
        } else{
            throw new UnprocesableException("faltan campos");
        }
        ProfesorOutputDTO out= new ProfesorOutputDTO(profesor);
        return out;
    }

    @Override
    public String borrarProfesor(String id) {
        profesorRepositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));
        profesorRepositorio.deleteById(id);

        return "Persona borrada";
    }

    @Override
    public ProfesorOutputDTO buscarID(String id) {
        ProfesorOutputDTO out=new ProfesorOutputDTO(profesorRepositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado")));
        return out;
    }

    @Override
    public ArrayList<ProfesorOutputDTO> buscarALL() {
        ArrayList<ProfesorOutputDTO> lista = new ArrayList<>();
        for(int i=0;i<profesorRepositorio.findAll().size();i++){
            System.out.println(profesorRepositorio.findAll().get(i));
            ProfesorOutputDTO out = new ProfesorOutputDTO(profesorRepositorio.findAll().get(i));
            lista.add(out);
        }

        return lista;
    }

    @Override
    public ProfesorOutputDTO modificar(ProfesorInputDTO input, String id) throws Exception {
        Persona persona = personaRepositorio.findById(input.getId_persona()).orElseThrow(()-> new NotFoundException("no encontrado"));
        System.out.println("esta es la persona " + persona);
        List<Estudiante> listaEstudiantes= new ArrayList<>();
        for(int i=0;i<input.getId_estudiante().length;i++){
            Estudiante estudiante = estudianteRepositorio.findById(input.getId_estudiante()[i]).orElseThrow(()-> new NotFoundException("no encontrado"));
            listaEstudiantes.add(estudiante);
        }
       // Estudiante estudiante = estudianteRepositorio.findById(input.getId_estudiante()).orElseThrow(()-> new NotFoundException("no encontrado"));

        Profesor profesor = new Profesor(input,persona, listaEstudiantes);
        profesor.setId(id);
        profesorRepositorio.save(profesor);
        ProfesorOutputDTO out = new ProfesorOutputDTO(profesor);
        return out;
    }

    @Override
    public ProfesorOutputDTO findProfesorbyID(String id) {
        Profesor profesor = profesorRepositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));
        return mapper.crearProfesorOutput(profesor);
    }

    @Override
    public Optional<Profesor> findById(String id) {
        return Optional.empty();
    }
}
