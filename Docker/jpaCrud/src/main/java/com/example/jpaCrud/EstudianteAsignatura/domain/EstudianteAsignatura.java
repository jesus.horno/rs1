package com.example.jpaCrud.EstudianteAsignatura.domain;


import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.input.EstudianteAsignaturaInputDTO;
import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Profesor.infraestructure.controller.dto.input.ProfesorInputDTO;
import com.example.jpaCrud.config.generador.StringPrefixedSequenceIdGenerator;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import java.util.Set;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@Getter
@Setter
@Entity
@NoArgsConstructor
public class EstudianteAsignatura {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ausencias_seq")
    @GenericGenerator(
            name = "ausencias_seq",
            strategy = "com.example.jpaCrud.config.generador.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "AUS"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%08d")
            })
    @Column(name = "id")
    private String id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_idEstudiante")
    private Estudiante estudianteCursandola;

//    @OneToOne
//    @JoinColumn(name = "fk_idPersona")
//    private Estudiante estudiante;

    @Column(name="numHours", nullable = false)
    private String asignatura;

    @Column(name="initial_date", nullable = false)
    private Date initial_date;

    @Column(name="finish_date")
    private Date finish_date;

    @Column(name="comments")
    private String coments;

    public EstudianteAsignatura(EstudianteAsignaturaInputDTO input, Estudiante estudiante){
        if(estudiante!=null) this.estudianteCursandola = estudiante;
        if(input.getAsignatura()!=null) this.asignatura=input.getAsignatura();
        if(input.getInitial_date()!=null) this.initial_date=input.getInitial_date();
        if(input.getFinish_date()!=null) this.finish_date= input.getFinish_date();;
        if(input.getComents()!=null) this.coments = input.getComents();
    }
}
