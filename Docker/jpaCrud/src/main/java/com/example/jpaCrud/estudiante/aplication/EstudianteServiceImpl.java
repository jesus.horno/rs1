package com.example.jpaCrud.Estudiante.aplication;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input.AsignarAsignaturasInputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.input.CreateEstudianteInputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.CreateEstudianteoOutputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.EstudianteAsignaturasAñadidasOutputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output.EstudiantePersonaOutputDTO;
import com.example.jpaCrud.Estudiante.infraestructure.repository.EstudianteRepositorio;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output.EstudianteAsignaturaOutputDTO;
import com.example.jpaCrud.EstudianteAsignatura.infraestructure.repository.EstudianteAsignaturaRepositorio;
import com.example.jpaCrud.Persona.domain.Persona;
import com.example.jpaCrud.Persona.infraestructure.repository.PersonaRepositorio;
import com.example.jpaCrud.Profesor.domain.Profesor;
import com.example.jpaCrud.Profesor.infraestructure.repository.ProfesorRepositorio;
import com.example.jpaCrud.config.error.NotFoundException;
import com.example.jpaCrud.config.error.UnprocesableException;
import com.example.jpaCrud.config.mapper.mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EstudianteServiceImpl implements EstudianteService{

    @Autowired
    EstudianteRepositorio repositorio;

    @Autowired
    PersonaRepositorio personaSer;

    @Autowired
    ProfesorRepositorio profesorRepositorio;

    @Autowired
    EstudianteAsignaturaRepositorio asignaturaRepositorio;

    @Autowired
    mapper map;

    public boolean comprobacionNulos(Estudiante estudiante){
        if(estudiante.getNum_hours_week() ==null ||estudiante.getBranch() == null) {
            System.out.println("es nulo 2");
            return false;
        }
        return true;
    }

    @Override
    public CreateEstudianteoOutputDTO añadirEstudiante(CreateEstudianteInputDTO input) throws NotFoundException {
        Persona persona = personaSer.findById(input.getId_persona()).orElseThrow(()-> new NotFoundException("no encontrado"));
        System.out.println("esta es la persona " + persona);
        Profesor profesor = profesorRepositorio.findById(input.getId_profesor()).orElseThrow(()-> new NotFoundException("no encontrado"));
        List<EstudianteAsignatura> listaAsignaturas= new ArrayList<>();
        for(int i=0;i<input.getId_asignaturas().length;i++){
            EstudianteAsignatura asignatura = asignaturaRepositorio.findById(input.getId_asignaturas()[i]).orElseThrow(()-> new NotFoundException("no encontrado"));
            listaAsignaturas.add(asignatura);
        }
        Estudiante estudiante = new Estudiante(input,persona,profesor,listaAsignaturas);
        if(comprobacionNulos(estudiante)) {
            repositorio.save(estudiante);
        } else{
            throw new UnprocesableException("faltan campos");
        }
        CreateEstudianteoOutputDTO out= new CreateEstudianteoOutputDTO(estudiante);

        return out;
    }

    @Override
    public String borrarEstudiante(String id) {

        repositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));
        repositorio.deleteById(id);

        return "Persona borrada";
    }

    @Override
    public CreateEstudianteoOutputDTO buscarID(String id, String output) {
        CreateEstudianteoOutputDTO out;
        System.out.println("ESTE ES EL OUTPUR"+ output);
        switch(output){
            case "full":
                return getTotal(id);
            default:
                return getParcial(id);
        }
    }

    @Override
    public ArrayList<CreateEstudianteoOutputDTO> buscarALL() {

        ArrayList<CreateEstudianteoOutputDTO> lista = new ArrayList<>();
        for(int i=0;i<repositorio.findAll().size();i++){
            System.out.println(repositorio.findAll().get(i));
            CreateEstudianteoOutputDTO out = new CreateEstudianteoOutputDTO(repositorio.findAll().get(i));
            lista.add(out);
        }

        return lista;
    }

    @Override
    public EstudiantePersonaOutputDTO getTotal(String id) {
        EstudiantePersonaOutputDTO out=new EstudiantePersonaOutputDTO(repositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado")));
        return out;
    }

    @Override
    public CreateEstudianteoOutputDTO getParcial(String id) {
        CreateEstudianteoOutputDTO out=new CreateEstudianteoOutputDTO(repositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado")));
        return out;
    }

    @Override
    public CreateEstudianteoOutputDTO modificar(CreateEstudianteInputDTO input, String id) throws Exception {
        Persona persona = personaSer.findById(input.getId_persona()).orElseThrow(()-> new NotFoundException("no encontrado"));
        System.out.println("esta es la persona " + persona);
        Profesor profesor = profesorRepositorio.findById(input.getId_profesor()).orElseThrow(()-> new NotFoundException("no encontrado"));
        List<EstudianteAsignatura> listaAsignaturas= new ArrayList<>();
        for(int i=0;i<input.getId_asignaturas().length;i++){
            EstudianteAsignatura asignatura = asignaturaRepositorio.findById(input.getId_asignaturas()[i]).orElseThrow(()-> new NotFoundException("no encontrado"));
            listaAsignaturas.add(asignatura);
        }
        Estudiante estudiante = new Estudiante(input,persona,profesor,listaAsignaturas);
        estudiante.setId(id);
        repositorio.save(estudiante);
        CreateEstudianteoOutputDTO out = new CreateEstudianteoOutputDTO(estudiante);
        return out;
    }

    @Override
    public CreateEstudianteoOutputDTO findEstudiantebyID(String id) {
        Estudiante estudiante= repositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));
        return map.crearEstudianteOutput(estudiante);
    }

    @Override
    public Optional<Estudiante> findById(String id) {
        return repositorio.findById(id);
    }

    @Override
    public EstudianteAsignaturasAñadidasOutputDTO AñadirAsignaturas(AsignarAsignaturasInputDTO input, String id) {

        Estudiante estudiante= repositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));
        List<EstudianteAsignatura> listaAsignaturas= new ArrayList<>();
        for(int i=0;i<input.getId_asignaturas().length;i++){
            EstudianteAsignatura asignatura = asignaturaRepositorio.findById(input.getId_asignaturas()[i]).orElseThrow(()-> new NotFoundException("no encontrado"));
            listaAsignaturas.add(asignatura);
        }
        System.out.println("esta es la asignatura FFFFFFFFFFFFFFFFFFFFFF" + listaAsignaturas);
        estudiante.setAñadirAsignaturas(listaAsignaturas);
        repositorio.save(estudiante);
        EstudianteAsignaturasAñadidasOutputDTO out = new EstudianteAsignaturasAñadidasOutputDTO(estudiante);
        return out;
    }

    @Override
    public EstudianteAsignaturasAñadidasOutputDTO EliminarAsignaturas(AsignarAsignaturasInputDTO input, String id) {
        Estudiante estudiante= repositorio.findById(id).orElseThrow(()-> new NotFoundException("no encontrado"));
        List<EstudianteAsignatura> listaAsignaturas= new ArrayList<>();
        for(int i=0;i<input.getId_asignaturas().length;i++){
            EstudianteAsignatura asignatura = asignaturaRepositorio.findById(input.getId_asignaturas()[i]).orElseThrow(()-> new NotFoundException("no encontrado"));
            listaAsignaturas.add(asignatura);
        }
        estudiante.setEliminarAsignaturas(listaAsignaturas);
        repositorio.save(estudiante);
        EstudianteAsignaturasAñadidasOutputDTO out = new EstudianteAsignaturasAñadidasOutputDTO(estudiante);
        return out;
    }

}
