package com.example.jpaCrud.EstudianteAsignatura.infraestructure.repository;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstudianteAsignaturaRepositorio extends JpaRepository<EstudianteAsignatura, String> {
}
