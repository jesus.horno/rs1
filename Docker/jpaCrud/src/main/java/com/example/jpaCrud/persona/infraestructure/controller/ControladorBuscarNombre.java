package com.example.jpaCrud.Persona.infraestructure.controller;

import com.example.jpaCrud.Persona.apllication.PersonaService;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
//mirar el request mapping
@RestController
public class ControladorBuscarNombre {
    @Autowired
    PersonaService service;

    @GetMapping("/asignacion/nombre/{nombre}")
    public ArrayList<CreatePersonaOutputDTO> getPersona(@PathVariable String nombre)  {
        return service.buscarNombre(nombre);
    }

}
