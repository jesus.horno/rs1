package com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.input;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import lombok.Data;

import javax.persistence.Column;
import java.util.Date;

@Data
public class EstudianteAsignaturaInputDTO {

    private String id_estudiante;
    private String asignatura;
    private Date initial_date;
    private Date finish_date;
    private String coments;
}
