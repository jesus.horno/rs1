package com.example.jpaCrud.Estudiante.infraestructure.controller.dto.output;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CreateEstudianteoOutputDTO {
    protected String id;
    protected int id_persona;
    protected Integer num_hours_week;
    protected String coments;
    protected String id_profesor;
    protected String branch;
    protected String[] asignaturas;

    public CreateEstudianteoOutputDTO(Estudiante estudiante){
        this.id=estudiante.getId();
        asignaturas=new String[estudiante.getAsignaturas().size()];
        for(int i=0; i< estudiante.getAsignaturas().size();i++){
            if(estudiante.getAsignaturas().get(i).getAsignatura()!=null)
                this.asignaturas[i] = estudiante.getAsignaturas().get(i).getId();
        }
        this.id_persona= estudiante.getPersona().getId_persona();
        this.id_profesor = estudiante.getProfesor().getId();
        this.coments=estudiante.getComents();
        this.num_hours_week = estudiante.getNum_hours_week();
        this.branch=estudiante.getBranch();
    }

}
