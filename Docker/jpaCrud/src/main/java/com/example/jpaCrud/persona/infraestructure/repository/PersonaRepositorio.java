package com.example.jpaCrud.Persona.infraestructure.repository;

import com.example.jpaCrud.Persona.domain.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface PersonaRepositorio extends JpaRepository<Persona, Integer> {
    public ArrayList<Persona> findByUsuario(String nombre);
}
