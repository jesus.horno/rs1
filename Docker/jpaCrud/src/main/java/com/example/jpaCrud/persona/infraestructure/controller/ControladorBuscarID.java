package com.example.jpaCrud.Persona.infraestructure.controller;


import com.example.jpaCrud.Persona.apllication.PersonaService;
import com.example.jpaCrud.Persona.infraestructure.controller.dto.output.CreatePersonaOutputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorBuscarID {

    @Autowired
    PersonaService service;

    @GetMapping("/asignacion/{id}")
    public CreatePersonaOutputDTO getPersona(@PathVariable int id) throws Exception {
        return service.buscarID(id);
    }
}
