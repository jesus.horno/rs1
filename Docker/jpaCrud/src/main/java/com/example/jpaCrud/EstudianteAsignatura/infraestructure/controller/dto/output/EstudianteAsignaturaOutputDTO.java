package com.example.jpaCrud.EstudianteAsignatura.infraestructure.controller.dto.output;

import com.example.jpaCrud.Estudiante.domain.Estudiante;
import com.example.jpaCrud.EstudianteAsignatura.domain.EstudianteAsignatura;
import com.example.jpaCrud.Profesor.domain.Profesor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class EstudianteAsignaturaOutputDTO {

    protected String id;
    protected String id_estudiante;
    protected String asignatura;
    protected Date initial_date;
    protected Date finish_date;
    protected String coments;

    public EstudianteAsignaturaOutputDTO(EstudianteAsignatura asignatura){
        this.id=asignatura.getId();
        this.id_estudiante = asignatura.getEstudianteCursandola().getId();
        this.asignatura=asignatura.getAsignatura();
        this.initial_date= asignatura.getInitial_date();
        this.finish_date= asignatura.getFinish_date();
        this.coments=asignatura.getComents();
    }
}
